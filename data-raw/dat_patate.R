## code to prepare `dat_patate` dataset goes here

dat_patate <- vroom::vroom("data-raw/potatoes_total_data_1900-2018_FILTERED.txt",
                           col_types = "fdddd")
usethis::use_data(dat_patate, overwrite = TRUE)